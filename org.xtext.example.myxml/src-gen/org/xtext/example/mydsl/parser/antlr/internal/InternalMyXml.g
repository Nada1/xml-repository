/*
* generated by Xtext
*/
grammar InternalMyXml;

options {
	superClass=AbstractInternalAntlrParser;
	
}

@lexer::header {
package org.xtext.example.mydsl.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyXmlGrammarAccess;

}

@parser::members {

 	private MyXmlGrammarAccess grammarAccess;
 	
    public InternalMyXmlParser(TokenStream input, MyXmlGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }
    
    @Override
    protected String getFirstRuleName() {
    	return "FXML";	
   	}
   	
   	@Override
   	protected MyXmlGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}
}

@rulecatch { 
    catch (RecognitionException re) { 
        recover(input,re); 
        appendSkippedTokens();
    } 
}




// Entry rule entryRuleFXML
entryRuleFXML returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFXMLRule()); }
	 iv_ruleFXML=ruleFXML 
	 { $current=$iv_ruleFXML.current; } 
	 EOF 
;

// Rule FXML
ruleFXML returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
	        newCompositeNode(grammarAccess.getFXMLAccess().getXmlDecXMLDecParserRuleCall_0_0()); 
	    }
		lv_xmlDec_0_0=ruleXMLDec		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFXMLRule());
	        }
       		set(
       			$current, 
       			"xmlDec",
        		lv_xmlDec_0_0, 
        		"XMLDec");
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getFXMLAccess().getProcessingInstructionsProcessingInstructionParserRuleCall_1_0()); 
	    }
		lv_processingInstructions_1_0=ruleProcessingInstruction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFXMLRule());
	        }
       		add(
       			$current, 
       			"processingInstructions",
        		lv_processingInstructions_1_0, 
        		"ProcessingInstruction");
	        afterParserOrEnumRuleCall();
	    }

)
)*(
(
		{ 
	        newCompositeNode(grammarAccess.getFXMLAccess().getRootElementElementDefinitionParserRuleCall_2_0()); 
	    }
		lv_rootElement_2_0=ruleElementDefinition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFXMLRule());
	        }
       		set(
       			$current, 
       			"rootElement",
        		lv_rootElement_2_0, 
        		"ElementDefinition");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleXMLDec
entryRuleXMLDec returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getXMLDecRule()); }
	 iv_ruleXMLDec=ruleXMLDec 
	 { $current=$iv_ruleXMLDec.current; } 
	 EOF 
;

// Rule XMLDec
ruleXMLDec returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='<?xml' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getXMLDecAccess().getXmlKeyword_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getXMLDecAccess().getPropsAttributePropertyDefinitionParserRuleCall_1_0()); 
	    }
		lv_props_1_0=ruleAttributePropertyDefinition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getXMLDecRule());
	        }
       		add(
       			$current, 
       			"props",
        		lv_props_1_0, 
        		"AttributePropertyDefinition");
	        afterParserOrEnumRuleCall();
	    }

)
)+	otherlv_2='?>' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getXMLDecAccess().getQuestionMarkGreaterThanSignKeyword_2());
    }
)
;





// Entry rule entryRuleProcessingInstruction
entryRuleProcessingInstruction returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getProcessingInstructionRule()); }
	 iv_ruleProcessingInstruction=ruleProcessingInstruction 
	 { $current=$iv_ruleProcessingInstruction.current; } 
	 EOF 
;

// Rule ProcessingInstruction
ruleProcessingInstruction returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='<?' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getProcessingInstructionAccess().getLessThanSignQuestionMarkKeyword_0());
    }
(
(
		lv_type_1_0=RULE_ID
		{
			newLeafNode(lv_type_1_0, grammarAccess.getProcessingInstructionAccess().getTypeIDTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getProcessingInstructionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"type",
        		lv_type_1_0, 
        		"ID");
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getProcessingInstructionAccess().getImportedNamespaceQualifiedNameWithWildCardParserRuleCall_2_0()); 
	    }
		lv_importedNamespace_2_0=ruleQualifiedNameWithWildCard		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getProcessingInstructionRule());
	        }
       		set(
       			$current, 
       			"importedNamespace",
        		lv_importedNamespace_2_0, 
        		"QualifiedNameWithWildCard");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_3='?>' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getProcessingInstructionAccess().getQuestionMarkGreaterThanSignKeyword_3());
    }
)
;





// Entry rule entryRuleElementDefinition
entryRuleElementDefinition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getElementDefinitionRule()); }
	 iv_ruleElementDefinition=ruleElementDefinition 
	 { $current=$iv_ruleElementDefinition.current; } 
	 EOF 
;

// Rule ElementDefinition
ruleElementDefinition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getElementDefinitionAccess().getContainerElementDefinitionParserRuleCall_0()); 
    }
    this_ContainerElementDefinition_0=ruleContainerElementDefinition
    { 
        $current = $this_ContainerElementDefinition_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getElementDefinitionAccess().getEmptyElementDefinitionParserRuleCall_1()); 
    }
    this_EmptyElementDefinition_1=ruleEmptyElementDefinition
    { 
        $current = $this_EmptyElementDefinition_1.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleContainerElementDefinition
entryRuleContainerElementDefinition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getContainerElementDefinitionRule()); }
	 iv_ruleContainerElementDefinition=ruleContainerElementDefinition 
	 { $current=$iv_ruleContainerElementDefinition.current; } 
	 EOF 
;

// Rule ContainerElementDefinition
ruleContainerElementDefinition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='<' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getContainerElementDefinitionAccess().getLessThanSignKeyword_0());
    }
((
(
		lv_namespace_1_0=RULE_ID
		{
			newLeafNode(lv_namespace_1_0, grammarAccess.getContainerElementDefinitionAccess().getNamespaceIDTerminalRuleCall_1_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getContainerElementDefinitionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"namespace",
        		lv_namespace_1_0, 
        		"ID");
	    }

)
)	otherlv_2=':' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getContainerElementDefinitionAccess().getColonKeyword_1_1());
    }
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getContainerElementDefinitionAccess().getNameQualifiedNameParserRuleCall_2_0()); 
	    }
		lv_name_3_0=ruleQualifiedName		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getContainerElementDefinitionRule());
	        }
       		set(
       			$current, 
       			"name",
        		lv_name_3_0, 
        		"QualifiedName");
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getContainerElementDefinitionAccess().getPropertiesAttributePropertyDefinitionParserRuleCall_3_0()); 
	    }
		lv_properties_4_0=ruleAttributePropertyDefinition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getContainerElementDefinitionRule());
	        }
       		add(
       			$current, 
       			"properties",
        		lv_properties_4_0, 
        		"AttributePropertyDefinition");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_5='>' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getContainerElementDefinitionAccess().getGreaterThanSignKeyword_4());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getContainerElementDefinitionAccess().getContentPCDataParserRuleCall_5_0()); 
	    }
		lv_content_6_0=rulePCData		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getContainerElementDefinitionRule());
	        }
       		set(
       			$current, 
       			"content",
        		lv_content_6_0, 
        		"PCData");
	        afterParserOrEnumRuleCall();
	    }

)
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getContainerElementDefinitionAccess().getChildrenElementDefinitionParserRuleCall_6_0()); 
	    }
		lv_children_7_0=ruleElementDefinition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getContainerElementDefinitionRule());
	        }
       		add(
       			$current, 
       			"children",
        		lv_children_7_0, 
        		"ElementDefinition");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_8='</' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getContainerElementDefinitionAccess().getLessThanSignSolidusKeyword_7());
    }
((
(
		lv_endnamespace_9_0=RULE_ID
		{
			newLeafNode(lv_endnamespace_9_0, grammarAccess.getContainerElementDefinitionAccess().getEndnamespaceIDTerminalRuleCall_8_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getContainerElementDefinitionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"endnamespace",
        		lv_endnamespace_9_0, 
        		"ID");
	    }

)
)	otherlv_10=':' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getContainerElementDefinitionAccess().getColonKeyword_8_1());
    }
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getContainerElementDefinitionAccess().getEndnameQualifiedNameParserRuleCall_9_0()); 
	    }
		lv_endname_11_0=ruleQualifiedName		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getContainerElementDefinitionRule());
	        }
       		set(
       			$current, 
       			"endname",
        		lv_endname_11_0, 
        		"QualifiedName");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_12='>' 
    {
    	newLeafNode(otherlv_12, grammarAccess.getContainerElementDefinitionAccess().getGreaterThanSignKeyword_10());
    }
)
;





// Entry rule entryRuleEmptyElementDefinition
entryRuleEmptyElementDefinition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getEmptyElementDefinitionRule()); }
	 iv_ruleEmptyElementDefinition=ruleEmptyElementDefinition 
	 { $current=$iv_ruleEmptyElementDefinition.current; } 
	 EOF 
;

// Rule EmptyElementDefinition
ruleEmptyElementDefinition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='<' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getEmptyElementDefinitionAccess().getLessThanSignKeyword_0());
    }
((
(
		lv_namespace_1_0=RULE_ID
		{
			newLeafNode(lv_namespace_1_0, grammarAccess.getEmptyElementDefinitionAccess().getNamespaceIDTerminalRuleCall_1_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getEmptyElementDefinitionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"namespace",
        		lv_namespace_1_0, 
        		"ID");
	    }

)
)	otherlv_2=':' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getEmptyElementDefinitionAccess().getColonKeyword_1_1());
    }
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getEmptyElementDefinitionAccess().getNameQualifiedNameParserRuleCall_2_0()); 
	    }
		lv_name_3_0=ruleQualifiedName		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getEmptyElementDefinitionRule());
	        }
       		set(
       			$current, 
       			"name",
        		lv_name_3_0, 
        		"QualifiedName");
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getEmptyElementDefinitionAccess().getPropsAttributePropertyDefinitionParserRuleCall_3_0()); 
	    }
		lv_props_4_0=ruleAttributePropertyDefinition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getEmptyElementDefinitionRule());
	        }
       		add(
       			$current, 
       			"props",
        		lv_props_4_0, 
        		"AttributePropertyDefinition");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_5='/>' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getEmptyElementDefinitionAccess().getSolidusGreaterThanSignKeyword_4());
    }
)
;





// Entry rule entryRulePCData
entryRulePCData returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getPCDataRule()); }
	 iv_rulePCData=rulePCData 
	 { $current=$iv_rulePCData.current; } 
	 EOF 
;

// Rule PCData
rulePCData returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getPCDataAccess().getContentContentParserRuleCall_0()); 
	    }
		lv_content_0_0=ruleContent		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getPCDataRule());
	        }
       		set(
       			$current, 
       			"content",
        		lv_content_0_0, 
        		"Content");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRuleAttributePropertyDefinition
entryRuleAttributePropertyDefinition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getAttributePropertyDefinitionRule()); }
	 iv_ruleAttributePropertyDefinition=ruleAttributePropertyDefinition 
	 { $current=$iv_ruleAttributePropertyDefinition.current; } 
	 EOF 
;

// Rule AttributePropertyDefinition
ruleAttributePropertyDefinition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(((
(
		lv_namespace_0_0=RULE_ID
		{
			newLeafNode(lv_namespace_0_0, grammarAccess.getAttributePropertyDefinitionAccess().getNamespaceIDTerminalRuleCall_0_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getAttributePropertyDefinitionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"namespace",
        		lv_namespace_0_0, 
        		"ID");
	    }

)
)	otherlv_1=':' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getAttributePropertyDefinitionAccess().getColonKeyword_0_1());
    }
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getAttributePropertyDefinitionAccess().getNameQualifiedNameParserRuleCall_1_0()); 
	    }
		lv_name_2_0=ruleQualifiedName		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getAttributePropertyDefinitionRule());
	        }
       		set(
       			$current, 
       			"name",
        		lv_name_2_0, 
        		"QualifiedName");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_3='=' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getAttributePropertyDefinitionAccess().getEqualsSignKeyword_2());
    }
(
(
		lv_value_4_0=RULE_STRING
		{
			newLeafNode(lv_value_4_0, grammarAccess.getAttributePropertyDefinitionAccess().getValueSTRINGTerminalRuleCall_3_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getAttributePropertyDefinitionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_4_0, 
        		"STRING");
	    }

)
))
;





// Entry rule entryRuleContent
entryRuleContent returns [String current=null] 
	@init { 
		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_ML_COMMENT");
	}
	:
	{ newCompositeNode(grammarAccess.getContentRule()); } 
	 iv_ruleContent=ruleContent 
	 { $current=$iv_ruleContent.current.getText(); }  
	 EOF 
;
finally {
	myHiddenTokenState.restore();
}

// Rule Content
ruleContent returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] 
    @init { enterRule(); 
		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_ML_COMMENT");
    }
    @after { leaveRule(); }:
((    this_ID_0=RULE_ID    {
		$current.merge(this_ID_0);
    }

    { 
    newLeafNode(this_ID_0, grammarAccess.getContentAccess().getIDTerminalRuleCall_0_0()); 
    }

    |    this_STRING_1=RULE_STRING    {
		$current.merge(this_STRING_1);
    }

    { 
    newLeafNode(this_STRING_1, grammarAccess.getContentAccess().getSTRINGTerminalRuleCall_0_1()); 
    }

    |    this_ANY_OTHER_2=RULE_ANY_OTHER    {
		$current.merge(this_ANY_OTHER_2);
    }

    { 
    newLeafNode(this_ANY_OTHER_2, grammarAccess.getContentAccess().getANY_OTHERTerminalRuleCall_0_2()); 
    }

    |
	kw='.' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getContentAccess().getFullStopKeyword_0_3()); 
    }

    |
	kw=':' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getContentAccess().getColonKeyword_0_4()); 
    }
)(    this_ID_5=RULE_ID    {
		$current.merge(this_ID_5);
    }

    { 
    newLeafNode(this_ID_5, grammarAccess.getContentAccess().getIDTerminalRuleCall_1_0()); 
    }

    |    this_STRING_6=RULE_STRING    {
		$current.merge(this_STRING_6);
    }

    { 
    newLeafNode(this_STRING_6, grammarAccess.getContentAccess().getSTRINGTerminalRuleCall_1_1()); 
    }

    |    this_ANY_OTHER_7=RULE_ANY_OTHER    {
		$current.merge(this_ANY_OTHER_7);
    }

    { 
    newLeafNode(this_ANY_OTHER_7, grammarAccess.getContentAccess().getANY_OTHERTerminalRuleCall_1_2()); 
    }

    |    this_WS_8=RULE_WS    {
		$current.merge(this_WS_8);
    }

    { 
    newLeafNode(this_WS_8, grammarAccess.getContentAccess().getWSTerminalRuleCall_1_3()); 
    }

    |
	kw='.' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getContentAccess().getFullStopKeyword_1_4()); 
    }

    |
	kw=':' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getContentAccess().getColonKeyword_1_5()); 
    }
)*)
    ;
finally {
	myHiddenTokenState.restore();
}





// Entry rule entryRuleQualifiedName
entryRuleQualifiedName returns [String current=null] 
	:
	{ newCompositeNode(grammarAccess.getQualifiedNameRule()); } 
	 iv_ruleQualifiedName=ruleQualifiedName 
	 { $current=$iv_ruleQualifiedName.current.getText(); }  
	 EOF 
;

// Rule QualifiedName
ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(    this_ID_0=RULE_ID    {
		$current.merge(this_ID_0);
    }

    { 
    newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
    }
(((
	'.' 
)=>
	kw='.' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
    }
)    this_ID_2=RULE_ID    {
		$current.merge(this_ID_2);
    }

    { 
    newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
    }
)*)
    ;





// Entry rule entryRuleQualifiedNameWithWildCard
entryRuleQualifiedNameWithWildCard returns [String current=null] 
	:
	{ newCompositeNode(grammarAccess.getQualifiedNameWithWildCardRule()); } 
	 iv_ruleQualifiedNameWithWildCard=ruleQualifiedNameWithWildCard 
	 { $current=$iv_ruleQualifiedNameWithWildCard.current.getText(); }  
	 EOF 
;

// Rule QualifiedNameWithWildCard
ruleQualifiedNameWithWildCard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getQualifiedNameWithWildCardAccess().getQualifiedNameParserRuleCall_0()); 
    }
    this_QualifiedName_0=ruleQualifiedName    {
		$current.merge(this_QualifiedName_0);
    }

    { 
        afterParserOrEnumRuleCall();
    }
(
	kw='.' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getQualifiedNameWithWildCardAccess().getFullStopKeyword_1_0()); 
    }

	kw='*' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getQualifiedNameWithWildCardAccess().getAsteriskKeyword_1_1()); 
    }
)?)
    ;





RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;

RULE_ML_COMMENT : '<!--' ( options {greedy=false;} : . )*'-->';

RULE_INT : ('0'..'9')+;

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;


