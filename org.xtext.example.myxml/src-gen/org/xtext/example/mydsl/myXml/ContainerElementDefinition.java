/**
 */
package org.xtext.example.mydsl.myXml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container Element Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getContent <em>Content</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getChildren <em>Children</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getEndnamespace <em>Endnamespace</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getEndname <em>Endname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getContainerElementDefinition()
 * @model
 * @generated
 */
public interface ContainerElementDefinition extends ElementDefinition
{
  /**
   * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myXml.AttributePropertyDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Properties</em>' containment reference list.
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getContainerElementDefinition_Properties()
   * @model containment="true"
   * @generated
   */
  EList<AttributePropertyDefinition> getProperties();

  /**
   * Returns the value of the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Content</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Content</em>' containment reference.
   * @see #setContent(PCData)
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getContainerElementDefinition_Content()
   * @model containment="true"
   * @generated
   */
  PCData getContent();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getContent <em>Content</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Content</em>' containment reference.
   * @see #getContent()
   * @generated
   */
  void setContent(PCData value);

  /**
   * Returns the value of the '<em><b>Children</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myXml.ElementDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Children</em>' containment reference list.
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getContainerElementDefinition_Children()
   * @model containment="true"
   * @generated
   */
  EList<ElementDefinition> getChildren();

  /**
   * Returns the value of the '<em><b>Endnamespace</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Endnamespace</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Endnamespace</em>' attribute.
   * @see #setEndnamespace(String)
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getContainerElementDefinition_Endnamespace()
   * @model
   * @generated
   */
  String getEndnamespace();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getEndnamespace <em>Endnamespace</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Endnamespace</em>' attribute.
   * @see #getEndnamespace()
   * @generated
   */
  void setEndnamespace(String value);

  /**
   * Returns the value of the '<em><b>Endname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Endname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Endname</em>' attribute.
   * @see #setEndname(String)
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getContainerElementDefinition_Endname()
   * @model
   * @generated
   */
  String getEndname();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myXml.ContainerElementDefinition#getEndname <em>Endname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Endname</em>' attribute.
   * @see #getEndname()
   * @generated
   */
  void setEndname(String value);

} // ContainerElementDefinition
