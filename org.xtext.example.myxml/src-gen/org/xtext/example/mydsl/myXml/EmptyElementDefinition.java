/**
 */
package org.xtext.example.mydsl.myXml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Empty Element Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myXml.EmptyElementDefinition#getProps <em>Props</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getEmptyElementDefinition()
 * @model
 * @generated
 */
public interface EmptyElementDefinition extends ElementDefinition
{
  /**
   * Returns the value of the '<em><b>Props</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myXml.AttributePropertyDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Props</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Props</em>' containment reference list.
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getEmptyElementDefinition_Props()
   * @model containment="true"
   * @generated
   */
  EList<AttributePropertyDefinition> getProps();

} // EmptyElementDefinition
