/**
 */
package org.xtext.example.mydsl.myXml.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.xtext.example.mydsl.myXml.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.myXml.MyXmlPackage
 * @generated
 */
public class MyXmlSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static MyXmlPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyXmlSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = MyXmlPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case MyXmlPackage.FXML:
      {
        FXML fxml = (FXML)theEObject;
        T result = caseFXML(fxml);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyXmlPackage.XML_DEC:
      {
        XMLDec xmlDec = (XMLDec)theEObject;
        T result = caseXMLDec(xmlDec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyXmlPackage.PROCESSING_INSTRUCTION:
      {
        ProcessingInstruction processingInstruction = (ProcessingInstruction)theEObject;
        T result = caseProcessingInstruction(processingInstruction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyXmlPackage.ELEMENT_DEFINITION:
      {
        ElementDefinition elementDefinition = (ElementDefinition)theEObject;
        T result = caseElementDefinition(elementDefinition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyXmlPackage.CONTAINER_ELEMENT_DEFINITION:
      {
        ContainerElementDefinition containerElementDefinition = (ContainerElementDefinition)theEObject;
        T result = caseContainerElementDefinition(containerElementDefinition);
        if (result == null) result = caseElementDefinition(containerElementDefinition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyXmlPackage.EMPTY_ELEMENT_DEFINITION:
      {
        EmptyElementDefinition emptyElementDefinition = (EmptyElementDefinition)theEObject;
        T result = caseEmptyElementDefinition(emptyElementDefinition);
        if (result == null) result = caseElementDefinition(emptyElementDefinition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyXmlPackage.PC_DATA:
      {
        PCData pcData = (PCData)theEObject;
        T result = casePCData(pcData);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyXmlPackage.ATTRIBUTE_PROPERTY_DEFINITION:
      {
        AttributePropertyDefinition attributePropertyDefinition = (AttributePropertyDefinition)theEObject;
        T result = caseAttributePropertyDefinition(attributePropertyDefinition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>FXML</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>FXML</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFXML(FXML object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>XML Dec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>XML Dec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseXMLDec(XMLDec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Processing Instruction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Processing Instruction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProcessingInstruction(ProcessingInstruction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Element Definition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Element Definition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElementDefinition(ElementDefinition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Container Element Definition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Container Element Definition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseContainerElementDefinition(ContainerElementDefinition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Empty Element Definition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Empty Element Definition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEmptyElementDefinition(EmptyElementDefinition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>PC Data</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>PC Data</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePCData(PCData object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Attribute Property Definition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Attribute Property Definition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAttributePropertyDefinition(AttributePropertyDefinition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //MyXmlSwitch
