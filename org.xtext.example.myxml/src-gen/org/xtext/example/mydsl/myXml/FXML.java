/**
 */
package org.xtext.example.mydsl.myXml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FXML</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myXml.FXML#getXmlDec <em>Xml Dec</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myXml.FXML#getProcessingInstructions <em>Processing Instructions</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myXml.FXML#getRootElement <em>Root Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getFXML()
 * @model
 * @generated
 */
public interface FXML extends EObject
{
  /**
   * Returns the value of the '<em><b>Xml Dec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Xml Dec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Xml Dec</em>' containment reference.
   * @see #setXmlDec(XMLDec)
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getFXML_XmlDec()
   * @model containment="true"
   * @generated
   */
  XMLDec getXmlDec();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myXml.FXML#getXmlDec <em>Xml Dec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Xml Dec</em>' containment reference.
   * @see #getXmlDec()
   * @generated
   */
  void setXmlDec(XMLDec value);

  /**
   * Returns the value of the '<em><b>Processing Instructions</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myXml.ProcessingInstruction}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Processing Instructions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Processing Instructions</em>' containment reference list.
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getFXML_ProcessingInstructions()
   * @model containment="true"
   * @generated
   */
  EList<ProcessingInstruction> getProcessingInstructions();

  /**
   * Returns the value of the '<em><b>Root Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Root Element</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Root Element</em>' containment reference.
   * @see #setRootElement(ElementDefinition)
   * @see org.xtext.example.mydsl.myXml.MyXmlPackage#getFXML_RootElement()
   * @model containment="true"
   * @generated
   */
  ElementDefinition getRootElement();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myXml.FXML#getRootElement <em>Root Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Root Element</em>' containment reference.
   * @see #getRootElement()
   * @generated
   */
  void setRootElement(ElementDefinition value);

} // FXML
