/**
 */
package org.xtext.example.mydsl.myXml.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.example.mydsl.myXml.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MyXmlFactoryImpl extends EFactoryImpl implements MyXmlFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static MyXmlFactory init()
  {
    try
    {
      MyXmlFactory theMyXmlFactory = (MyXmlFactory)EPackage.Registry.INSTANCE.getEFactory(MyXmlPackage.eNS_URI);
      if (theMyXmlFactory != null)
      {
        return theMyXmlFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new MyXmlFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyXmlFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case MyXmlPackage.FXML: return createFXML();
      case MyXmlPackage.XML_DEC: return createXMLDec();
      case MyXmlPackage.PROCESSING_INSTRUCTION: return createProcessingInstruction();
      case MyXmlPackage.ELEMENT_DEFINITION: return createElementDefinition();
      case MyXmlPackage.CONTAINER_ELEMENT_DEFINITION: return createContainerElementDefinition();
      case MyXmlPackage.EMPTY_ELEMENT_DEFINITION: return createEmptyElementDefinition();
      case MyXmlPackage.PC_DATA: return createPCData();
      case MyXmlPackage.ATTRIBUTE_PROPERTY_DEFINITION: return createAttributePropertyDefinition();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FXML createFXML()
  {
    FXMLImpl fxml = new FXMLImpl();
    return fxml;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XMLDec createXMLDec()
  {
    XMLDecImpl xmlDec = new XMLDecImpl();
    return xmlDec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcessingInstruction createProcessingInstruction()
  {
    ProcessingInstructionImpl processingInstruction = new ProcessingInstructionImpl();
    return processingInstruction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ElementDefinition createElementDefinition()
  {
    ElementDefinitionImpl elementDefinition = new ElementDefinitionImpl();
    return elementDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ContainerElementDefinition createContainerElementDefinition()
  {
    ContainerElementDefinitionImpl containerElementDefinition = new ContainerElementDefinitionImpl();
    return containerElementDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EmptyElementDefinition createEmptyElementDefinition()
  {
    EmptyElementDefinitionImpl emptyElementDefinition = new EmptyElementDefinitionImpl();
    return emptyElementDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PCData createPCData()
  {
    PCDataImpl pcData = new PCDataImpl();
    return pcData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AttributePropertyDefinition createAttributePropertyDefinition()
  {
    AttributePropertyDefinitionImpl attributePropertyDefinition = new AttributePropertyDefinitionImpl();
    return attributePropertyDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyXmlPackage getMyXmlPackage()
  {
    return (MyXmlPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static MyXmlPackage getPackage()
  {
    return MyXmlPackage.eINSTANCE;
  }

} //MyXmlFactoryImpl
